<?php
/**
 * @package TrustyCorePlugin
 */
/*
Plugin Name: Trusty Core Plugin
Plugin URI: http://softsandila.com/
Description:
Version: 1.0.0
Author: Zaheer Bashir
License: GPLv2 or later
Text Domain: trusty-core-plugin
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2020 SoftSandila.
*/

defined('ABSPATH') or die('Hey, what are you doing here ! You are silly human');

if ( file_exists( dirname(__FILE__) . '/vendor/autoload.php' ) ) {
	require_once dirname(__FILE__ ) . '/vendor/autoload.php';
}

function activate(){
	TrustyCore\Inc\Base\Activate::activate();
}

function deactivate(){
	TrustyCore\Inc\Base\Deactivate::deactivate();
}

register_activation_hook(__FILE__ , 'activate');
register_deactivation_hook(__FILE__ , 'deactivate');

if ( class_exists( 'TrustyCore\Inc\\Init') ) {
	TrustyCore\Inc\Init::register_services();
}
