<?php
/**
 * @package TrustyCorePlugin
 */

namespace TrustyCore\Inc\Base;

use \TrustyCore\Inc\Base\BaseController;

class SettingsLinks extends BaseController {

	public function register() {
		add_filter( "plugin_action_links_$this->plugin", array( $this, 'settings_links' ) );
	}

	function settings_links( $links ) {
		$settings_link = '<a href="options-general.php?page=test_plugin">Settings</a>';
		array_push( $links, $settings_link );

		return $links;
	}
}
