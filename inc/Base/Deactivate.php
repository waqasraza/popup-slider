<?php
/**
 * @package TrustyCorePlugin
 */

namespace TrustyCore\Inc\Base;

class Deactivate {

	public static function deactivate(){
		flush_rewrite_rules();
	}
}

