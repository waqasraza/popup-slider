<?php
/**
 * @package TrustyCorePlugin
 */

namespace TrustyCore\Inc\Pages;

use TrustyCore\Inc\Base\BaseController;
use TrustyCore\Inc\Api\SettingsApi;

class Admin extends BaseController {
	public $settings;

	public function __construct() {
		$this->settings = new SettingsApi();
	}

	public function register() {

		$pages = [
			[
			'page_title' => 'Popup Plugin',
			'menu_title' => 'PopupPlugin',
			'capability' => 'manage_options',
			'menu_slug' => 'popup_plugin',
			'callback' => function() { echo '<h1>Popup Plugin</h1>';},
			'icon_url' => 'dashicons-store',
			'position' => 110
			],
			[
			'page_title' => 'Slider Plugin',
			'menu_title' => 'SliderPlugin',
			'capability' => 'manage_options',
			'menu_slug' => 'slider_plugin',
			'callback' => function(){ echo '<h1>Slider Plugin</h1>';},
			'icon_url' => 'dashicons-store',
			'position' => 111
			]
		];

		$this->settings->addPages( $pages )->register();

	}
}