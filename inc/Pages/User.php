<?php
/**
 * @package TrustyCorePlugin
 */

namespace TrustyCore\Inc\Pages;

use TrustyCore\Inc\Base\BaseController;

class User extends BaseController {

    function register() {
        add_shortcode( 'popup_shortcode', array ( $this , 'popup_shortcode_handler' ) );
        add_action( 'template_redirect', array ( $this , 'popup_modals_redirect_post' ) );
    }



    function popup_modals_redirect_post() {
        $queried_post_type = get_query_var('post_type');
        if ( is_single() && 'popup_modals' ==  $queried_post_type ) {
            wp_redirect( home_url(), 301 );
            exit;
        }
    }

    function popup_shortcode_handler(){
        ?>
        <style>
            div.smallBox {
                width: 150px !important;
                margin-top: 150px;
                left: auto;
            }
            #largeModal {
                height: 400px !important;
                margin-top: 150px;
            }
            @media (max-width: 767px) {
                #largeModal{
                    margin-top: 150px;
                    right: 10%;
                    left: 10%;
                    overflow: initial;
                }
            }
        </style>        <?php
            $options = get_option( 'myhome_redux' );
            $modal_id = $options['mh-popup-modals'];
            
            $modal_data   = get_post( $modal_id );
            $featured_image = get_the_post_thumbnail( $modal_data->ID , 'thumbnail' );

        ?>
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">

                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?php echo $modal_data->post_title; ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                            <?php echo $modal_data->post_content; ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade bd-example-modal-sm smallBox" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <?php echo $featured_image;?>
                    </div>
                </div>
            </div>

<!--            <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target=".bd-example-modal-lg" id="button1">Modal One</button>-->
<!---->
<!--            <div class="modal fade bd-example-modal-l" id="newModal" style="margin-top: 150px;" data-keyboard="false" data-backdrop="static" role="dialog">-->
<!--                <div class="modal-dialog">-->
<!---->
<!--                    <div class="modal-content">-->
<!--                        <div class="modal-header">-->
<!--                            <h4 class="modal-title">--><?php //echo $modal_data->post_title; ?><!--</h4>-->
<!--                        </div>-->
<!--                        <div class="modal-body">-->
<!--                            --><?php //echo $modal_data->post_content; ?>
<!--                        </div>-->
<!--                        <div class="modal-footer">-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
<!--            </div>-->
        <?php
    }


}