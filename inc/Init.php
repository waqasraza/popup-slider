<?php
/**
 * @package TrustyCorePlugin
 */

namespace TrustyCore\Inc;

final class Init{
	/**
	 *  return full list of classes
	 */
	public static function get_services(){
		return [
			Pages\User::class,
			Base\Enqueue::class,
			Base\SettingsLinks::class,
		];
	}

	/**
	 *  Loop through the classes and intialize them,
	 * call  the register() method and if it exists
	 *
	 */

	public static function register_services(){
		foreach ( self::get_services() as $class ) {
			$service = self::instantiate( $class );
			if ( method_exists( $service , 'register' ) ) {
				$service->register();
			}
		}
	}

	private function instantiate( $class ){
		$service = new $class();
		return $service;
	}


}


//
//
//
//use Inc\Activate;
//use Inc\Deactivate;
//use Inc\Shortcode;
//
//
//if ( !class_exists( 'PopupPlugin') ) {
//	class PopupPlugin
//	{
//		public $plugin;
//		function __construct() {
//			$this->plugin = plugin_basename(__FILE__);
//			add_action( 'wp_enqueue_script'  , array ( $this , 'enqueue') );
//		}
//
//		// methods

//		function settings_links ( $links ) {
//			$settings_link = '<a href="options-general.php?page=test_plugin">Settings</a>';
//			array_push( $links , $settings_link );
//			return $links;
//		}
//		function enqueue()
//		{
//			wp_enqueue_script( 'popup-script', plugin_dir_path( __FILE__ ) . '/assets/js/popup-script.js', array(), '1.0.0', true );
//		}
//
//		function activate()
//		{
//			Activate::activate();
//		}
//
//		function deactivate()
//		{
//			Deactivate::deactivate();
//		}
//
//		function uninstall()
//		{
//			// delete CPT
//			//flush rewrite rules
//			flush_rewrite_rules();
//		}
//	}
//
//	$popup_plugin = new PopupPlugin();
//	$popup_plugin->register();
//
//	// activation hook
//	register_activation_hook(__FILE__, array('Activate', 'activate'));
//
//	// deactivation hook
//
//	register_deactivation_hook(__FILE__, array('Deactivate', 'deactivate'));
//}

