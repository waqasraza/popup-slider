(function ($) {
    $(function () {
        $('body').click(function (event)
        {
            if($(event.target).closest('#largeModal').length && $(event.target).is('#largeModal')) {
                $("#largeModal").modal({
                    show: false,
                    backdrop: false
                });
                $('#smallModal').modal({
                    show: true,
                    backdrop: false
                });
            }
            if ( $(window).width() >= 320 && $(window).width() <= 767 ) {
               $("#largeModal").click(function () {
                   $("#smallModal").modal('show');
                   $(this).modal('hide');
               });
            }
        });

        $(window).load(function () {
            $('#largeModal').modal({
                show : true,
                backdrop: true
            });
            $('#smallModal').modal({
                show : false,
                backdrop: false
            });
        });
        $('#smallModal').click(function () {
            $('#largeModal').modal({
                show : true,
                backdrop: true
            });
            $(this).modal('hide');
        });


    });
})(jQuery);